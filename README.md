# xp-vhd
A simple script that generates a Windows XP VHD disk image from [Windows XP Mode](https://www.microsoft.com/es-co/download/details.aspx?id=8002) to virtualize.

### Prerequisites
Debian:
```
$ sudo apt-get install p7zip-full
```
Fedora:
```
$ sudo dnf install p7zip
```
### Running
```
$ ./xp-vhd
```
This will generates the Windows XP VHD disk "VirtualXP.vhd"

### Troubleshooting
* After running the image with any virtualization software, you may need to force shootdown and relauch the virtual machine.
* This is a clean VHD image, so you may need to install some drivers.
